#!/usr/bin/env bash

command_exists() { type "$1" &>/dev/null; }

chmod 700 ./distro-specific.bash
./distro-specific.bash

cd install-stuff/
chmod 700 ./*.bash
./install-claws-mail.bash
./install-fish.bash
./install-misc.sh
./install-qemu-libvirt.bash
./install-rust.sh
./install-steam.bash
# must go after steam
./install-librewolf.bash
./install-vs-code.bash
./install-fonts.bash
./install-yt-music.bash
./install-vesktop.sh
./install-swayfx.bash
./install-podman.sh
cd ..

./configure-zram.bash

cd ./dotfiles
chmod 700 run-distro-de-script.sh
./run-distro-de-script.sh
chmod 700 restore.sh
./restore.sh
cd ..

./device-specific.sh

if command_exists "rpm-ostree"; then
    read -p "Press enter to reboot"
    reboot
fi
