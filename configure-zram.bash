#!/usr/bin/env bash
set -euo pipefail

if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

# only fedora is up-to-date

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo apt-get install kitty -y
    echo "Please install SchildiChat, nvim/neovim"
elif command_exists "dnf"; then
    sudo dnf remove zram-generator -y
    echo 'zram' | sudo tee /etc/modules-load.d/zram.conf
    echo 'ACTION=="add", KERNEL=="zram0", ATTR{initstate}=="0", ATTR{comp_algorithm}="zstd", ATTR{disksize}="48G", RUN="/usr/bin/mkswap -U clear %N", TAG+="systemd"' | sudo tee /etc/udev/rules.d/99-zram.rules
    zram_fstab='/dev/zram0 none swap defaults,discard,pri=160 0 0'
    if ! grep -q "$zram_fstab" /etc/fstab; then
        echo "$zram_fstab" | sudo tee -a /etc/fstab
    fi
    echo '@reboot bash -c "mkswap -U clear /dev/zram0; swapon --discard --priority 160 /dev/zram0"' | sudo crontab -
fi
