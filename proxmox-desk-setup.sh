#!/usr/bin/env bash
# switch to no subscription repos
echo '# Proxmox VE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve bookworm pve-no-subscription' >/etc/apt/sources.list.d/pve-no-enterprise.list
echo 'deb http://download.proxmox.com/debian/ceph-squid bookworm no-subscription' >/etc/apt/sources.list.d/ceph.list
rm /etc/apt/sources.list.d/pve-enterprise.list
apt update
apt upgrade

echo 'zram' >/etc/modules-load.d/zram.conf
echo 'ACTION=="add", KERNEL=="zram0", ATTR{comp_algorithm}="zstd", ATTR{disksize}="64G", RUN="/usr/bin/mkswap -U clear /dev/%k", TAG+="systemd"' >/etc/udev/rules.d/99-zram.rules
echo '/dev/zram0 none swap defaults,discard,pri=100 0 0' >/etc/fstab
echo 'vm.swappiness = 180
vm.watermark_boost_factor = 0
vm.watermark_scale_factor = 125
vm.page-cluster = 0' >/etc/sysctl.d/99-vm-zram-parameters.conf
