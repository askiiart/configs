#!/usr/bin/env bash
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo curl -L https://askiiart.net/repos/debian/dists/bookworm/stable/binary-amd64/askiiart.list -o /etc/apt/sources.list.d/askiiart.list
    sudo apt update --allow-insecure-repositories
    sudo apt install youtube-music -y --allow-unauthenticated
elif command_exists "yum"; then
    sudo dnf config-manager addrepo --from-repofile 'https://askiiart.net/repos/fedora/x86_64/askiiart.repo'
    sudo dnf install youtube-music -y
elif command_exists "rpm-ostree"; then
    sudo wget https://askiiart.net/repos/fedora/x86_64/askiiart.repo -O /etc/yum.repos.d/askiiart.repo
    rpm-ostree install youtube-music -y
elif command_exists "yay"; then
    yay -S youtube-music-bin --noconfirm --needed
elif command_exists "xbps-install"; then
    cd ~/Applications
    curl -LO $(curl -s https://api.github.com/repos/th-ch/youtube-music/releases/latest | grep "browser_download_url.*YouTube-Music-.*.AppImage" | cut -d : -f 2,3 | tr -d \") -C -
    cd -
else
    echo "IDK, check the repo: https://github.com/th-ch/youtube-music"
fi
