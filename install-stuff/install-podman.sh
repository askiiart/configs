#!/usr/bin/env bash

command_exists() { type "$1" &>/dev/null; }

if command_exists "dnf"; then
    #sudo dnf install podman -y
    #systemctl enable --now --user podman

    # install docker-compose
    sudo dnf remove podman podman-docker -y
    sudo dnf config-manager addrepo --from-repofile https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf install docker-ce{,-cli,-rootless-extras}

    # rootless
    sudo dnf install fuse-overlayfs
    sudo sh -eux <<EOF
# Load ip_tables module
modprobe ip_tables
EOF
    dockerd-rootless-setuptool.sh install
fi
