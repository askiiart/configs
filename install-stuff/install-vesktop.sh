#!/usr/bin/env bash
set -euo pipefail

if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    curl -LO $(curl -s https://api.github.com/repos/Vencord/Vesktop/releases/latest | grep "vesktop_.*_amd64.deb" | head --lines 1 | cut -d : -f 2,3 | tr -d \")
    sudo apt-get install ./vesktop_*.deb -y
elif command_exists "dnf"; then
    sudo dnf install $(curl -s https://api.github.com/repos/Vencord/Vesktop/releases/latest | grep "browser_download_url.*vesktop-.*.x86_64.rpm" | head --lines 1 | cut -d : -f 2,3 | tr -d \")
elif command_exists "rpm-ostree"; then
    rpm-ostree install $(curl -s https://api.github.com/repos/Vencord/Vesktop/releases/latest | grep "browser_download_url.*vesktop-.*.x86_64.rpm" | head --lines 1 | cut -d : -f 2,3 | tr -d \")
elif command_exists "yay"; then
    yay -S vesktop-bin --noconfirm --needed
elif command_exists "xbps-install"; then
    cd ~/Applications
    curl -LO $(curl -s https://api.github.com/repos/Vencord/Vesktop/releases/latest | grep "browser_download_url.*Vesktop-.*.AppImage" | tail --lines 1 | cut -d : -f 2,3 | tr -d \")
    cd -
else
    echo "IDK, check the repo: https://github.com/Vencord/Vesktop"
fi
