#!/usr/bin/env bash
set -e
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo apt-get install qemu-system libvirt-daemon-system virt-manager -y
elif command_exists "yum"; then
    sudo yum install @virtualization virt-manager -y
    sudo systemctl enable --now libvirtd.service
    sudo usermod -aG libvirt $(whoami)
elif command_exists "rpm-ostree"; then
    if ! command_exists "libvirtd"; then
        rpm-ostree install virt-install libvirt-daemon-config-network libvirt-daemon-kvm qemu-kvm virt-manager virt-viewer
        read -p "Press enter to reboot, then run do-everything.bash again"
        reboot
    else
        sudo systemctl enable --now libvirtd.service
    fi
elif command_exists "pacman"; then
    sudo pacman -S qemu-full dnsmasq --noconfirm --needed
    sudo pacman -S virt-manager --noconfirm --needed
    sudo systemctl enable --now libvirtd.service
elif command_exists "zypp"; then
    # Untested
    sudo zypper install qemu -y
elif command_exists "emerge"; then
    sudo echo Not yet supported, exiting...
elif command_exists "apk"; then
    sudo apk add qemu-img qemu-system-x86_64 libvirt-daemon py-libvirt py-libxml2
    sudo apk add git
    sudo rc-update add libvirtd
    sudo rc-service libvirtd start
elif command_exists "xbps-install"; then
    sudo xbps-install qemu libvirt virt-manager
    sudo usermod -aG libvirt $(whoami)
    sudo ln -s /etc/sv/libvirtd/ /var/service/
else
    echo "Unsupported: unknown package manager and distro"
fi
