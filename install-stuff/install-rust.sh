#!/usr/bin/env bash
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "yum"; then
    sudo dnf install rustup -y
    fish -c "rustup-init"

    fish -c "cargo install cavif"
    sudo dnf remove rust -y
elif command_exists "rpm-ostree"; then
    sudo wget https://askiiart.net/repos/fedora/x86_64/askiiart.repo -O /etc/yum.repos.d/askiiart.repo
    rpm-ostree install rustup -y
    echo "run rustup-init yourself or update the script"
elif command_exists "yay"; then
    yay -S rustup --noconfirm --needed
else
    echo "figure out rustup yourself"
    echo "enter to continue"
    read -p ""
fi
