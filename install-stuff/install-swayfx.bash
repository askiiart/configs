#!/usr/bin/env bash
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

# only fedora is up-to-date

command_exists() { type "$1" &>/dev/null; }

if command_exists "dnf"; then
    sudo dnf copr enable swayfx/swayfx
    sudo rm /etc/dnf/protected.d/fedora-sway.conf
    sudo dnf install swayfx --allowerasing  # conflicts with sway, so this will remove sway
fi
