#!/usr/bin/env bash
set -e

if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo apt-get install fish -y
elif command_exists "yum"; then
    sudo yum install fish -y
elif command_exists "rpm-ostree" && ! command_exists "fish"; then
    rpm-ostree install fish -y
    read -p "Press enter to reboot, then run do-everything.bash again"
    reboot
elif command_exists "pacman"; then
    sudo pacman -S fish --noconfirm
elif command_exists "zypp"; then
    # Untested
    sudo zypper install fish -y
elif command_exists "emerge"; then
    sudo echo Not yet supported, exiting...
    exit
elif command_exists "apk"; then
    sudo apk add fish
elif command_exists "xbps-install"; then
    sudo xbps-install fish-shell -y
else
    echo "Unsupported: unknown package manager and distro"
    exit
fi

chsh -s $(readlink -f $(which fish))
