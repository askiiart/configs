#!/usr/bin/env bash
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

# only fedora is up-to-date

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo apt-get install kitty -y
    echo "Please install SchildiChat, nvim/neovim"
elif command_exists "dnf"; then
    sudo dnf install kitty neovim gcc gnome-hexgl drawing make vlc freeglut ncdu gcolor3 rustup p7zip mosh krita podman podman-docker podman-compose gajim progress yt-dlp genisoimage zstd iperf3 keepassxc python3-pip ffmpegthumbnailer linuxconsoletools bind-utils hyfetch fastfetch nmap unrar-free restic gparted bsdtar android-tools httpd-tools digikam edid-decode hexchat htop minetest fuse-sshfs syncthing nasm sysstat openshot HandBrake HandBrake-gui electrum feather obs-studio fuse-libs pandoc wtype fedora-packager rpmdevtools createrepo_c iotop igt-gpu-tools -y
    sudo dnf install ffmpeg --allowerasing

    sudo systemctl enable --now syncthing@askiiart

    sudo dnf copr enable wezfurlong/wezterm-nightly
    sudo dnf install wezterm

    # Install schildichat-desktop
    # should be in my repo, but this works as both a fallback, and is faster than downloading from my repo - updates will come from my repo, though
    sudo dnf install $(curl -s https://api.github.com/repos/SchildiChat/schildichat-desktop/releases/latest | grep "browser_download_url.*schildichat-desktop-.*.x86_64.rpm" | cut -d : -f 2,3 | tr -d \" | tr -d ' ')

    ### Install AppImages ###
    # Install FjordLauncherUnlocked
    mkdir ~/Applications
    cd ~/Applications
    curl -LO $(curl -s https://api.github.com/repos/hero-persson/FjordLauncherUnlocked/releases/latest | grep "browser_download_url.*FjordLauncher-.*.AppImage" | head --lines 1 | cut -d : -f 2,3 | tr -d \") -C -
    # Install localsend
    curl -LO $(curl -s https://api.github.com/repos/localsend/localsend/releases/latest | grep "browser_download_url.*LocalSend-.*.AppImage" | head --lines 1 | cut -d : -f 2,3 | tr -d \") -C -

    # Install DwarFS
    sudo dnf copr enable jc141/DwarFS
    sudo dnf install dwarfs

    sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

    # Add my repo
    sudo dnf config-manager addrepo --from-repofile 'https://askiiart.net/repos/fedora/x86_64/askiiart.repo'
    sudo dnf install polycat -y
elif command_exists "rpm-ostree"; then
    rpm-ostree install kitty neovim gcc gnome-hexgl drawing make vlc freeglut ncdu gcolor3 rustup clippy p7zip mosh krita podman podman-docker podman-compose gajim progress yt-dlp genisoimage zstd iperf3 -y --allow-inactive

    # Install Prism Launcher
    sudo wget https://copr.fedorainfracloud.org/coprs/g3tchoo/prismlauncher/repo/fedora-$(rpm -E %fedora)/g3tchoo-prismlauncher-fedora-$(rpm -E %fedora).repo -O /etc/yum.repos.d/g3tchoo-prismlauncher.repo
    rpm-ostree install prismlauncher-qt5

    flatpak install me.iepure.devtoolbox
elif command_exists "yay"; then
    yay -S kitty neovim gcc vlc ncdu gcolor3 rustup p7zip mosh krita podman podman-compose gajim progress yt-dlp genisoimage drawing clippy gnome-hexgl zstd iperf3 unzip schildichat-desktop-bin digikam man-db prismlauncher-qt5-bin progress bind-utils --noconfirm --needed
elif command_exists "zypp"; then
    # Untested
    sudo zypper install kitty -y
    echo "Please install SchildiChat, nvim/neovim"
elif command_exists "emerge"; then
    echo Not yet supported, exiting...
    exit
elif command_exists "apk"; then
    echo Not yet supported, exiting...
elif command_exists "xbps-install"; then
    # missing: hexgl, drawing, clippy, schildichat, genisoimage
    # not included: libfreeglut, don't think that's required
    sudo xbps-install kitty neovim gcc vlc ncdu gcolor3 rustup p7zip mosh krita podman podman-compose gajim progress yt-dlp zstd iperf3 unzip
    cd ~/Applications
    curl -LO $(curl -s https://api.github.com/repos/SchildiChat/schildichat-desktop/releases/latest | grep "browser_download_url.*SchildiChat-.*.AppImage" | head --lines 1 | cut -d : -f 2,3 | tr -d \") -C -
    cd -
else
    echo "Unsupported: unknown package manager and distro"
    exit
fi
