#!/usr/bin/env bash

command_exists() { type "$1" &>/dev/null; }

# on void i could totally install the nerd fonts via `xbps-install nerd-fonts-otf` but i am NOT installing an extra 1.45-ish GB of the rest of the nerd fonts along with it
# stock firacode is available via `font-firacode` though
mkdir -p ~/.local/share/fonts/firacode
mkdir ./tmp-fonts
cd ./tmp-fonts

curl $(curl -s https://api.github.com/repos/ryanoasis/nerd-fonts/releases/latest | grep "browser_download_url.*FiraCode.zip" | cut -d : -f 2,3 | tr -d \") -LO
unzip FiraCode.zip
mv FiraCodeNerdFont*.ttf  ~/.local/share/fonts/firacode/

if command_exists "xbps-install"; then
    sudo xbps-install font-atkinson-hyperlegible-otf
else
    mkdir -p ~/.local/share/fonts/atkinson-hyperlegible
    curl -LO https://raw.githubusercontent.com/googlefonts/atkinson-hyperlegible/main/fonts/ttf/AtkinsonHyperlegible-Regular.ttf
    curl -LO https://raw.githubusercontent.com/googlefonts/atkinson-hyperlegible/main/fonts/ttf/AtkinsonHyperlegible-Bold.ttf
    curl -LO https://raw.githubusercontent.com/googlefonts/atkinson-hyperlegible/main/fonts/ttf/AtkinsonHyperlegible-Italic.ttf
    curl -LO https://raw.githubusercontent.com/googlefonts/atkinson-hyperlegible/main/fonts/ttf/AtkinsonHyperlegible-BoldItalic.ttf
    mv AtkinsonHyperlegible*.ttf ~/.local/share/fonts/atkinson-hyperlegible/
fi

cd -
rm -rf ./tmp-fonts
