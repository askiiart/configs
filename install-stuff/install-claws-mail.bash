#!/usr/bin/env bash

if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "yay"; then
    yay -S claws-mail python-gpgme nuspell aspell aspell-en spamassassin
    sudo systemctl enable --now spamassassin.service
    cd ~
    git clone https://git.askiiart.net/askiiart/gpg-email-helper
    cd -
elif command_exists "dnf"; then
    sudo dnf install spamassassin claws-mail claws-mail-plugins-pgp claws-mail-plugins-spamassassin claws-mail-plugins-rssyl
    sudo systemctl enable --now spamassassin.service
    cd ~
    git clone https://git.askiiart.net/askiiart/gpg-email-helper
    cd -
elif command_exists "rpm-ostree"; then
    if ! command_exists "spamassassin"; then
        rpm-ostree install spamassassin claws-mail claws-mail-plugins-pgp claws-mail-plugins-spamassassin claws-mail-plugins-rssyl
        read -p "Press enter to reboot, then run do-everything.bash again"
        reboot
    else
        sudo systemctl enable --now spamassassin.service
        cd ~
        git clone https://git.askiiart.net/askiiart/gpg-email-helper
        cd -
    fi
elif command_exists "xbps-install"; then
    sudo xbps-install claws-mail spamassassin
    ln -s /etc/sv/spamd /var/service/
    sv start spamd
else
    echo "IDK"
fi
