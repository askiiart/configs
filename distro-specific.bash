#!/usr/bin/env bash
if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if command_exists "apt-get"; then
    sudo apt update
    sudo apt install curl -y

elif command_exists "dnf"; then
    sudo dnf config-manager addrepo --from-repofile 'https://askiiart.net/repos/fedora/x86_64/askiiart.repo'
    sudo dnf remove libreoffice* atril -y
    sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y

    # disable firewall (poorly)
    sudo firewall-cmd --permanent --zone=trusted --set-target=ACCEPT
    lines=$(netstat -i | wc -l)
    for interface in $(netstat -i | cut -f 1 -d ' ' | tail --lines $((lines - 2))); do
        sudo firewall-cmd --zone=trusted --change-interface=$interface
    done

    # grub setup
    grep -qxF 'GRUB_DISABLE_OSPROBER=false' /etc/default/grub || echo 'GRUB_DISABLE_OSPROBER=false' | sudo tee -a /etc/default/grub
elif command_exists "rpm-ostree"; then
    sudo wget https://askiiart.net/repos/fedora/x86_64/askiiart.repo -O /etc/yum.repos.d/askiiart.repo
    rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
    sudo rpm-ostree apply-live --allow-replacement
    flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

elif command_exists "pacman"; then
    WD=$(pwd)
    sudo pacman -S --noconfirm --needed git base-devel
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
    cd $WD
    rm -rf yay
    # get fast arch mirrors
    yay -S rate-mirrors-bin
    rate-mirrors --disable-comments-in-file --entry-country=US --protocol=https arch --max-delay 7200 | sudo tee /etc/pacman.d/mirrorlist
    yay -S noto-fonts-emoji --noconfirm --needed

elif command_exists "zypp"; then
    echo "not yet implemented"

elif command_exists "emerge"; then
    echo "not yet implemented"

elif command_exists "apk"; then
    echo "not yet implemented"

elif command_exists "xbps-install"; then
    sudo xbps-install NetworkManager dbus polkit polkit-gnome nano noto-fonts-emoji noto-fonts-cjk python3 python3-pip
    mkdir ~/Applications
    cd ~/Applications
    curl -LO $(curl -s https://api.github.com/repos/TheAssassin/AppImageLauncher/releases/latest | grep "browser_download_url.*appimagelauncher-lite.*-x86_64.AppImage" | cut -d : -f 2,3 | tr -d \") -C -
    chmod +x appimagelauncher-lite*-x86_64.AppImage
    cd -
    echo "AppImageLauncher is in ~/Applications, deal with it yourself or update this script"
    sudo xbps-install void-repo-multilib void-repo-nonfree

else
    echo "Unsupported: unknown package manager and distro"

fi
