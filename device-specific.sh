#!/usr/bin/env bash

command_exists() { type "$1" &>/dev/null; }

if grep -q "Shyvana" /sys/devices/virtual/dmi/id/product_name; then
    cd /tmp/
    git clone https://github.com/WeirdTreeThing/chromebook-linux-audio --depth 1
    cd ./chromebook-linux-audio/
    ./setup-audio
    cd -
    rm -rf /tmp/chromebook-linux-audio/
elif grep -q "HP ProBook 440 G7" /sys/devices/virtual/dmi/id/product_name; then
    if command_exists "dnf"; then
        sudo dnf install akmod-nvidia xorg-x11-drv-nvidia-cuda vulkan xorg-x11-drv-nvidia-cuda-libs
        sudo dnf mark user akmod-nvidia

        # build nvidia module for all kernels
        for item in $(rpm -qa kernel); do
            item=${item#???????}
            sudo dnf install kernel-$item kernel-devel-$item -y
            sudo akmods --kernels $item --rebuild
        done

        # load modules early, otherwise the display manager loads first
        # update: it doesn't detect them sooooo just removing the display manager instead
        #echo 'force_drivers+=" nvidia nvidia_modeset nvidia_uvm nvidia_drm "' | sudo tee /etc/dracut.conf.d/nvidia.conf
        sudo dracut --regenerate-all -f

        sudo dnf remove sddm

        # start sway with `--unsupported-gpu`
        sudo sed -i 's/Exec=start-sway$/Exec=start-sway --unsupported-gpu/g' /usr/share/wayland-sessions/sway.desktop
    else
        echo "missing nvidia drivers"
    fi
fi
